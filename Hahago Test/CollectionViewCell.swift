//
//  CollectionViewCell.swift
//  Hahago Test
//
//  Created by NWT_mac01 on 2018/3/19.
//  Copyright © 2018年 NWT. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
