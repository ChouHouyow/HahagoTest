//
//  ViewController.swift
//  Hahago Test
//
//  Created by NWT_mac01 on 2018/3/19.
//  Copyright © 2018年 NWT. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class ViewController: UIViewController {
   
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var groupView: UICollectionView!
    @IBOutlet weak var friendCount: UILabel!
    @IBOutlet weak var groupCount: UILabel!
    
    var ref:DatabaseReference!
    var users = [User]()
    var group = [User]()
    var SearchUsers = [User]()
    var isSearching = false
    var searchController:UISearchController! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        getFirebaseData()
        listenToFirebase()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }

    
    func setupNavBar(){
        
        //建立 SearchController 時，傳入 nil 代表不另外開一個 TableView 來顯示
        //而是與本來的資料共用一個 TableView 來顯示
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self // 設置代理
        // 取消背景變深
        searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false // 搜尋滾動不消失
    }
    
    
    
    
    
    //取得第一次資料，後續得到資料將由listenToFirebase監聽
    func getFirebaseData(){
        
        
        ref = Database.database().reference() //取得database資源
        //取得users的全部資料
        ref.child("users").observe(.childAdded, with: { (snapshot) in
        
            if let dictionary = snapshot.value as? [String:AnyObject]{
                let user = User()
                
                //這是讀取全部，如果key不對會crash
                //user.setValuesForKeys(dictionary)
                
                //針對單一key
                user.f_uid   = dictionary["f_uid"] as? String
                user.name    = dictionary["name"] as? String
                user.hasjoin = dictionary["hasjoin"] as? Bool
                user.uid     = dictionary["uid"]  as? String
                user.photo   = dictionary["photo"]  as? String
                self.users.append(user)
                
                if user.hasjoin!{
                    self.group.append(user)
                }
                    
               //更新畫面
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                    self.groupView.reloadData()
                    self.friendCount.text = String(self.users.count)
                    self.groupCount.text  = String(self.group.count)
                }
            }
            
        }){ (error) in
            print(error.localizedDescription)
        }
        
        
//單一人資料
//        ref.child("users").child("mytdkfW8DkW6Dk5KivtZrISqRFX2").observeSingleEvent(of: .value, with: { (snapshot) in
//            // Get user value
//            let value = snapshot.value as? NSDictionary
//            let username = value?["name"] as? String ?? ""
//            print(username)
//        }) { (error) in
//            print(error.localizedDescription)
//        }
        
    }
    
    
    
    func listenToFirebase(){
        
        ref.child("users").observe(.childChanged, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                let user = User()
            
                
                
                user.f_uid   = dictionary["f_uid"] as? String
                user.name    = dictionary["name"] as? String
                user.hasjoin = dictionary["hasjoin"] as? Bool
                user.uid     = dictionary["uid"]  as? String
                user.photo   = dictionary["photo"]  as? String
                
                
                
                
                //當群組狀態改變更新SearchUsers即users值
                //避免下指標只改其中一個
                if let Searchindex = self.SearchUsers.index(where: { $0.f_uid == user.f_uid }) {
                
                    self.SearchUsers[Searchindex] = user
                    
                }
                
                if let normalindex = self.users.index(where: { $0.f_uid == user.f_uid }) {
                    
                    self.users[normalindex] = user
                    
                }
               self.tableview.reloadData()
            }
            
        }){ (error) in
            print(error.localizedDescription)
        }
    }
}






extension ViewController:UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UISearchBarDelegate{
  
    
    /*＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
     *   成員顯示
     *＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if group.count == 0{
            return group.count+1
        }else{
            return group.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewcell", for: indexPath) as! CollectionViewCell
        
        if group.count == 0{
            
            cell.photo.image = UIImage(named:"add")
            cell.name.text   = ""
            
        }else{
            
            
            let user  = group[indexPath.row]
            cell.name.text   = user.name
            
            
            //照片
            let url = URL(string:user.photo!)
            if let image = url?.cachedImage { //抓過了 -> 直接顯示
                cell.photo.image = image
                cell.photo.layer.cornerRadius  = cell.photo.frame.width/2
                cell.photo.layer.masksToBounds = true
            } else { //沒抓過 ->下載圖片
                cell.photo.alpha = 0
                // 下載圖片
                url?.fetchImage { image in
                    cell.photo.image = image
                    cell.photo.layer.cornerRadius  = cell.photo.frame.width/2
                    cell.photo.layer.masksToBounds = true
                    UIView.animate(withDuration: 0.3) {
                        cell.photo.alpha = 1
                    }
                }
            }
        }
        
        return cell
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: groupView.frame.height)
    }
    
    
    
    
    
    
    /*＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
     *   好友顯示
     *＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return SearchUsers.count
        }else{
            return users.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! TableViewCell
        
        var user  = users[indexPath.row]
        self.friendCount.text = String(self.users.count)
        if isSearching {
            user  = SearchUsers[indexPath.row]
            self.friendCount.text = String(self.SearchUsers.count)
        }
        
        
        cell.name.text      = user.name
        cell.check.isHidden = !user.hasjoin!
        cell.uid.text       = user.uid
        

        let url = URL(string:user.photo!)
        if let image = url?.cachedImage { //抓過了 -> 直接顯示
            cell.photo.image = image
            cell.photo.layer.cornerRadius  = cell.photo.frame.width/2
            cell.photo.layer.masksToBounds = true
        } else { //沒抓過 ->下載圖片
            cell.photo.alpha = 0
            // 下載圖片
            url?.fetchImage { image in
                cell.photo.image = image
                cell.photo.layer.cornerRadius  = cell.photo.frame.width/2
                cell.photo.layer.masksToBounds = true
                UIView.animate(withDuration: 0.3) {
                    cell.photo.alpha = 1
                }
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var user  = users[indexPath.row]
       
        //如果搜索中，則資料轉為 SearchUsers
        if isSearching{
            user  = SearchUsers[indexPath.row]
        }

        if user.hasjoin!{
            self.ref.child("users/"+user.f_uid!).child("hasjoin").setValue(false)
            
            //如果不在group則在group array中移除
            if let index = self.group.index(where: { $0.f_uid == user.f_uid }) {
                self.group.remove(at:index)
                self.groupCount.text  = String(self.group.count)
                self.groupView.reloadData()
            }
            
        }else{
            self.ref.child("users/"+user.f_uid!).child("hasjoin").setValue(true)
            self.group.append(user)
            self.groupCount.text  = String(self.group.count)
            self.groupView.reloadData()
        }
    }
    
    
    
    
    
    
    /*＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
     *   抽尋功能
     *＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊*/

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        SearchUsers = users.filter({ (user) -> Bool in
            return (user.name?.contains(searchText))!
        })
        self.tableview.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        self.friendCount.text = String(self.users.count)
        self.tableview.reloadData()
    }
    // 輸入框取得focus時
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        self.friendCount.text = String(self.SearchUsers.count)
        self.tableview.reloadData()
    }
    
    // 輸入框失去focus時
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}


